package com.thenewmotion.kmreg;

import akka.camel.CamelMessage;
import akka.camel.javaapi.UntypedConsumerActor;

import java.util.Map;

public class DequeingActor extends UntypedConsumerActor {
    
    public void onReceive(Object message) {
        CamelMessage camelMessage = (CamelMessage) message;
        System.out.println(">>>>>>>>>>>>>> receiving message from queue: " + message);
        System.out.println(">>>>>>>>>>>>>> receiving message from queue: " + camelMessage.getHeaders());
        
        for (Map.Entry<String, Object> next : camelMessage.getHeaders().entrySet()) {
            System.out.println(">>>>>>>>>>> header: " + next.getKey() + ":" + next.getValue());
        }

    }

    @Override
    public String getEndpointUri() {
        return "spring-amqp:thenewmotion:lovetoload:request?type=direct";
    }

}
