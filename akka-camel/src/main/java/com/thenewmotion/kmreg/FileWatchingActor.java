package com.thenewmotion.kmreg;

import akka.camel.javaapi.UntypedConsumerActor;


public class FileWatchingActor extends UntypedConsumerActor {
    
    public void onReceive(Object message) {
        System.out.println(message);
      }

    @Override
    public String getEndpointUri() {
        return "file:/tmp/recordme";
    }

}
