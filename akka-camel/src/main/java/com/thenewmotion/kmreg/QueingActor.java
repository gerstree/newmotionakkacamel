package com.thenewmotion.kmreg;

import akka.camel.javaapi.UntypedProducerActor;


public class QueingActor extends UntypedProducerActor {

    @Override
    public String getEndpointUri() {
        return "spring-amqp:thenewmotion:pass-registered";
    }

}
