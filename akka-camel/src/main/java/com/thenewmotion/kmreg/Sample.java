package com.thenewmotion.kmreg;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.camel.Camel;
import akka.camel.CamelExtension;
import akka.kernel.Bootable;
import amqp.spring.camel.component.SpringAMQPComponent;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;


public class Sample implements Bootable {

    final ActorSystem system = ActorSystem.create("kmreg");

    public void shutdown() {}

    public void startup() {
        CachingConnectionFactory cf = new CachingConnectionFactory();
        cf.setHost("127.0.0.1");
 
        Camel camel = CamelExtension.get(system);
        SpringAMQPComponent component = camel.context().getComponent("spring-amqp", SpringAMQPComponent.class);
        component.setConnectionFactory(cf);

        ActorRef producingActor = system.actorOf(new Props(QueingActor.class));
        //ActorRef consumingActor = system.actorOf(new Props(DequeingActor.class));
        
        
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            producingActor.tell("{ \"cid\": \"String\", \"passnumber\": \"String\", \"email\": \"String\", \"originalMSP\": \"String\" }", null);
        }
        
    }

}
