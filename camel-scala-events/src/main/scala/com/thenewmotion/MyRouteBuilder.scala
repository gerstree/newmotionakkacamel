package com.thenewmotion

import org.apache.camel.Exchange
import org.apache.camel.scala.dsl.builder.RouteBuilder

import org.apache.camel.{ Exchange, Processor }


/**
 * A Camel Router using the Scala DSL
 */
class MyRouteBuilder extends RouteBuilder {

    // val ENDPOINT_URL = "http://192.168.173.248:8080/api/integration/chargepass?authMethod=Basic&authUsername=some&authPassword=v%I,6AeKM2"
    val ENDPOINT_URL = "http://10.0.2.9:8081/api/integration/chargepass?authMethod=Basic&authUsername=some&authPassword=v%I,6AeKM2"

    // an example of a Processor method
   val myProcessorMethod = (exchange: Exchange) => {
     exchange.getIn.setBody("block test")
   }
   
    "spring-amqp:thenewmotion:events-truus:pass-registered" --> "direct:lovetoload"

    "direct:lovetoload"
        .setHeader("Exchange.HTTP_METHOD", "POST")
        .setHeader("Content-Type", "application/json")
        .to(ENDPOINT_URL)

    "spring-amqp:thenewmotion:events-lovetoload:invitiation-accepted" --> "direct:truus"

    "direct:truus" --> "log:com.thenewmotion?level=DEBUG"

    /**
   .process(new Processor() {
               def process(exchange: Exchange) {
               exchange.getOut.setBody("""{ "cid": "String", "passnumber": "String", "email": "String", "originalMSP": "String" }""")
           }
       })
   */

}
