package com.thenewmotion

import org.apache.camel.main.Main
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.scala.dsl.builder.RouteBuilderSupport
import amqp.spring.camel.component.SpringAMQPComponent

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;


/**
 * A Main to run Camel with MyRouteBuilder
 */
object MyRouteMain extends RouteBuilderSupport {

    def main(args: Array[String]) {

        val camelContext = new DefaultCamelContext();

        val cf = new CachingConnectionFactory();
        cf.setHost("127.0.0.1");
        val component = camelContext.getComponent("spring-amqp", classOf[SpringAMQPComponent]);
        component.setConnectionFactory(cf);

        camelContext.addRoutes(new MyRouteBuilder())

        camelContext.start();
    }
}

